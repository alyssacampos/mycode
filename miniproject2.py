#!/usr/bin/env python3

# Guess zodiac sign based off birth date.

day = input("Input birthday: ")
month = input("Input birth month (ex: june, july..): ")

# For every individual month put by user will output a response with the matched zodiac sign.

if month == 'december':
    astro_sign = 'Sagittarius :(' if (day < 22) else 'capricorn'
elif month == 'january':
    astro_sign = 'Capricorn :(' if  (day < 20) else 'aquarius'
elif month == 'february':
    astro_sign = 'Aquarius :(' if (day < 19) else  'pisces'
elif month == 'march':
    astro_sign == 'Pisces :(' if (day < 21) else 'aries'
elif month == 'april':
    astro_sign = 'Aries :(' if (day < 20) else 'taurus'
elif month == 'may':
    astro_sign = 'Taurus :(' if (day < 21) else 'gemini'
elif month == 'june':
    astro_sign = 'Gemini :(' if (day < 21) else 'cancer'
elif month == 'july':
    astro_sign = 'Cancer' if (day < 23) else 'leo'
elif month == 'august':
    astro_sign = 'leo :(' if (day <23) else 'virgo'
elif month == 'september':
    astro_sign = 'Virgo:(' if (day < 23) else 'libra'
elif month == 'october':
    astro_sign = 'Libra :(' if (day < 23) else 'scorpio'
elif month  == 'november':
    astro_sign = 'Scorpio' if (day < 22) else 'sagittarius'
print("Your Zodiac sign is :",astro_sign)   #to display the zodiac sign.

while False:
    print('Goodbye')
    break

