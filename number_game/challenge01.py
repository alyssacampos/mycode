#!/usr/bin/env python3
"""Number guessing game! User has 5 chances to guess a number between 1 and 50!"""

def main():
    num= random.randint(1,50)

    rounds= 0

    while rounds < 5 and guess != num:
        guess= input("Guess a number between 1 and 50\n>")

        # COOL CODE ALERT: what is the purpose of the next four lines?
        if guess.isdigit():
            guess= int(guess)
        else:
            continue

        if guess > num:
            print("A little lower!")
            rounds + 2

        if guess < num:
            print("A little higher!")
            rounds + 2

        else:
            print("Winner!")

